###system

**system model**
**Para sacar información sobre el system model**
```
[hw@nvidia-t4 ~]$ sudo dmidecode -t system
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 3.0.0 present.

Handle 0x0001, DMI type 1, 27 bytes
System Information
	Manufacturer: Supermicro
	Product Name: X10SRA-F
	Version: 0123456789
	Serial Number: 0123456789
	UUID: 00000000-0000-0000-0000-ffffffffffff
	Wake-up Type: Power Switch
	SKU Number: Default string
	Family: Default string

Handle 0x0023, DMI type 12, 5 bytes
System Configuration Options
	Option 1: Default string

Handle 0x0024, DMI type 32, 20 bytes
System Boot Information
	Status: No errors detected

Handle 0x0066, DMI type 15, 73 bytes
System Event Log
	Area Length: 65535 bytes
	Header Start Offset: 0x0000
	Header Length: 16 bytes
	Data Start Offset: 0x0010
	Access Method: Memory-mapped physical 32-bit address
	Access Address: 0xFF430000
	Status: Valid, Not Full
	Change Token: 0x00000001
	Header Format: Type 1
	Supported Log Type Descriptors: 25
	Descriptor 1: Single-bit ECC memory error
	Data Format 1: Multiple-event handle
	Descriptor 2: Multi-bit ECC memory error
	Data Format 2: Multiple-event handle
	Descriptor 3: Parity memory error
	Data Format 3: None
	Descriptor 4: Bus timeout
	Data Format 4: None
	Descriptor 5: I/O channel block
	Data Format 5: None
	Descriptor 6: Software NMI
	Data Format 6: None
	Descriptor 7: POST memory resize
	Data Format 7: None
	Descriptor 8: POST error
	Data Format 8: POST results bitmap
	Descriptor 9: PCI parity error
	Data Format 9: Multiple-event handle
	Descriptor 10: PCI system error
	Data Format 10: Multiple-event handle
	Descriptor 11: CPU failure
	Data Format 11: None
	Descriptor 12: EISA failsafe timer timeout
	Data Format 12: None
	Descriptor 13: Correctable memory log disabled
	Data Format 13: None
	Descriptor 14: Logging disabled
	Data Format 14: None
	Descriptor 15: System limit exceeded
	Data Format 15: None
	Descriptor 16: Asynchronous hardware timer expired
	Data Format 16: None
	Descriptor 17: System configuration information
	Data Format 17: None
	Descriptor 18: Hard disk information
	Data Format 18: None
	Descriptor 19: System reconfigured
	Data Format 19: None
	Descriptor 20: Uncorrectable CPU-complex error
	Data Format 20: None
	Descriptor 21: Log area reset/cleared
	Data Format 21: None
	Descriptor 22: System boot
	Data Format 22: None
	Descriptor 23: End of log
	Data Format 23: None
	Descriptor 24: OEM-specific
	Data Format 24: OEM-specific
	Descriptor 25: OEM-specific
	Data Format 25: OEM-specific 
```


**how old is the hardware**

**mainboard model**
**Esta saida te dice especificamente el nombre del producto y la marca:*
```
[hw@nvidia-t4 ~]$ sudo dmidecode -s baseboard-manufacturer
Supermicro
[hw@nvidia-t4 ~]$ sudo dmidecode -s baseboard-product-name
X10SRA-F
```
 **bios version and bios date**
 ```
 [hw@nvidia-t4 ~]$ sudo dmidecode -t bios
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 3.0.0 present.

Handle 0x0000, DMI type 0, 24 bytes
BIOS Information
	Vendor: American Megatrends Inc.
	Version: 2.0c
	Release Date: 09/25/2017
	Address: 0xF0000
	Runtime Size: 64 kB
	ROM Size: 16 MB
	Characteristics:
```		
 
**link to manual**
[https://www.supermicro.com/support/resources/results.cfm]


**^link to product**
[https://www.amazon.es/Supermicro-X10SRA-F-Intel-Socket-Placa/dp/B00O7ZK10S]

memory banks (free or occupied)





**how many disks and types can be connected**
Esta salida te dice los discos que hay conectados:
```
[hw@nvidia-t4 ~]$ lsscsi
[0:0:0:0]    disk    ATA      KINGSTON SHFS37A BBF0  /dev/sda 
[3:0:0:0]    disk    ATA      KINGSTON SHFS37A BBF0  /dev/sdb
```
chipset, link to

cpu

cpu model, year, cores, threads, cache

socket

pci

number of pci slots, lanes available

devices connected

network device, model, kernel module, speed

audio device, model, kernel module

vga device, model, kernel module

hard disks

/dev/* , model, bus type, bus speed

test fio random (IOPS) and sequential (MBps)