Extract hardware info from host with linux

###SYSTEM
Para sacar el modelo del sistema:
**system model**
```
[root@f32-isard ~]# dmidecode -t system
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0100, DMI type 1, 27 bytes
System Information
	Manufacturer: QEMU
	Product Name: Standard PC (Q35 + ICH9, 2009)
	Version: pc-q35-5.1
	Serial Number: Not Specified
	UUID: acf6166f-f23b-4030-9ae8-2374e807cea9
	Wake-up Type: Power Switch
	SKU Number: Not Specified
	Family: Not Specified

Handle 0x2000, DMI type 32, 11 bytes
System Boot Information
	Status: No errors detected
```
Para sacar la fecha y versión:
**bios version and bios date** 
```
[root@f32-isard ~]# dmidecode -t bios
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0000, DMI type 0, 24 bytes
BIOS Information
	Vendor: SeaBIOS
	Version: rel-1.13.0-48-gd9c812dda519-prebuilt.qemu.org
	Release Date: 04/01/2014
	Address: 0xE8000
	Runtime Size: 96 kB
	ROM Size: 64 kB
	Characteristics:
		BIOS characteristics not supported
		Targeted content distribution is supported
	BIOS Revision: 0.0
```



how old is the hardware?

###MAINBOARD
**mainboard model**
Para extraer información sobre el modelo de la placa base y el nombre de la placa base:
Esta es la marca de la placa base:
```
[root@a25 ~]# dmidecode -s baseboard-manufacturer
Gigabyte Technology Co., Ltd.
```
Este es el nombre del producto:
```
[root@a25 ~]# dmidecode -s baseboard-product-name
H81M-S2PV
```

**link to manual**
Este link es el que te envia al manual de la placa base:
[http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#support-manual]

**link to product**
Este link es el que te envia a ver el producto:
[http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf]

memory banks (free or occupied)



how many disks and types can be connected


chipset, link to
[https://ark.intel.com/content/www/es/es/ark/products/75016/intel-h81-chipset.html]

###CPU

**cpu model**
se usa dmidecode para ver la versión y modelo de la cpu:
```
[root@a25 ~]# dmidecode -s processor-version
Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
```
```
[root@a25 ~]# dmidecode -s processor-manufacturer
Intel
```

**year**
Esta fecha la he encontrado buscando en un navegador, especificamente en la web oficial de Intel:Este link es el que me a dado la información del año:
[https://ark.intel.com/content/www/es/es/ark/products/83538/intel-pentium-processor-g3250-3m-cache-3-20-ghz.html]
Q3'14






**cores, threads,socket,cache**
Este comando lista toda la cpu: 
```
[root@a25 ~]# lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          2
On-line CPU(s) list:             0,1
Thread(s) per core:              1
Core(s) per socket:              2
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           60
Model name:                      Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Stepping:                        3
CPU MHz:                         1338.547
CPU max MHz:                     3200.0000
CPU min MHz:                     800.0000
BogoMIPS:                        6385.11
Virtualization:                  VT-x
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB

```


###PCI

**number of pci slots, lanes available**
```
[root@a25 ~]# dmidecode -t 9
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0020, DMI type 9, 17 bytes
System Slot Information
	Designation: J6B2
	Type: x16 PCI Express
	Current Usage: In Use
	Length: Long
	ID: 0
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:02.0

Handle 0x0021, DMI type 9, 17 bytes
System Slot Information
	Designation: J6B1
	Type: x1 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 1
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:1c.0

Handle 0x0022, DMI type 9, 17 bytes
System Slot Information
	Designation: J6D1
	Type: x8 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 2
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:01.0

Handle 0x0023, DMI type 9, 17 bytes
System Slot Information
	Designation: J7B1
	Type: x16 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 3
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:03.0

Handle 0x0024, DMI type 9, 17 bytes
System Slot Information
	Designation: J8B4
	Type: x1 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 4
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:1c.7

Handle 0x0025, DMI type 9, 17 bytes
System Slot Information
	Designation: J8B3
	Type: 32-bit PCI
	Current Usage: In Use
	Length: Short
	ID: 6
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:14:1e.0
```
**devices connected**

**network device, model, kernel module, speed**

audio device, model, kernel module

vga device, model, kernel module

hard disks

/dev/* , model, bus type, bus speed

test fio random (IOPS) and sequential (MBps)