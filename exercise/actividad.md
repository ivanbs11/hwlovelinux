**23 de noviembre**

Tarjetas pci conectadas
```
[root@sysrescue ~]# lspci -nn
00:00.0 Host bridge [0600]: Intel Corporation Xeon E3-1200 v2/3rd Gen Core processor DRAM Controller [8086:0150] (rev 09)
00:02.0 VGA compatible controller [0300]: Intel Corporation Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller [8086:0152] (rev 09)
00:16.0 Communication controller [0780]: Intel Corporation 6 Series/C200 Series Chipset Family MEI Controller #1 [8086:1c3a] (rev 04)
00:1a.0 USB controller [0c03]: Intel Corporation 6 Series/C200 Series Chipset Family USB Enhanced Host Controller #2 [8086:1c2d] (rev 05)
00:1b.0 Audio device [0403]: Intel Corporation 6 Series/C200 Series Chipset Family High Definition Audio Controller [8086:1c20] (rev 05)
00:1c.0 PCI bridge [0604]: Intel Corporation 6 Series/C200 Series Chipset Family PCI Express Root Port 1 [8086:1c10] (rev b5)
00:1c.4 PCI bridge [0604]: Intel Corporation 6 Series/C200 Series Chipset Family PCI Express Root Port 5 [8086:1c18] (rev b5)
00:1c.5 PCI bridge [0604]: Intel Corporation 82801 PCI Bridge [8086:244e] (rev b5)
00:1d.0 USB controller [0c03]: Intel Corporation 6 Series/C200 Series Chipset Family USB Enhanced Host Controller #1 [8086:1c26] (rev 05)
00:1f.0 ISA bridge [0601]: Intel Corporation H61 Express Chipset LPC Controller [8086:1c5c] (rev 05)
00:1f.2 IDE interface [0101]: Intel Corporation 6 Series/C200 Series Chipset Family Desktop SATA Controller (IDE mode, ports 0-3) [8086:1c00] (rev 05)
00:1f.3 SMBus [0c05]: Intel Corporation 6 Series/C200 Series Chipset Family SMBus Controller [8086:1c22] (rev 05)
00:1f.5 IDE interface [0101]: Intel Corporation 6 Series/C200 Series Chipset Family Desktop SATA Controller (IDE mode, ports 4-5) [8086:1c08] (rev 05)
01:00.0 Ethernet controller [0200]: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller [10ec:8168] (rev 06)
02:00.0 Ethernet controller [0200]: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller [10ec:8168] (rev 06)
03:00.0 PCI bridge [0604]: Intel Corporation 82801 PCI Bridge [8086:244e] (rev 41)
04:00.0 Ethernet controller [0200]: Realtek Semiconductor Co., Ltd. RTL-8100/8101L/8139 PCI Fast Ethernet Adapter [10ec:8139] (rev 10)
04:01.0 Network controller [0280]: Ralink corp. RT2561/RT61 802.11g PCI [1814:0301]
```
lspci -nn -k -mm -v     Mas info máás especifica


**Model dels disc i capacitats**
```
[root@sysrescue ~]# df
Filesystem     1K-blocks   Used Available Use% Mounted on
dev              1934448      0   1934448   0% /dev
run              1960980  95784   1865196   5% /run
httpspace        2941476 641796   2299680  22% /run/archiso/httpspace
copytoram        2941476 641792   2299684  22% /run/archiso/copytoram
cowspace          980492    968    979524   1% /run/archiso/cowspace
/dev/loop0        642048 642048         0 100% /run/archiso/sfs/airootfs
airootfs          980492    968    979524   1% /
tmpfs            1960980      0   1960980   0% /dev/shm
tmpfs               4096      0      4096   0% /sys/fs/cgroup
tmpfs            1960984      0   1960984   0% /tmp
tmpfs            1960980   2056   1958924   1% /etc/pacman.d/gnupg
tmpfs             392196     16    392180   1% /run/user/0
```
```
[root@sysrescue ~]# lsblk -o MODEL,NAME
MODEL             NAME
                  loop0
ST500DM002-1BD142 sda
                  ├─sda1
                  ├─sda2
                  ├─sda3
                  ├─sda4
                  ├─sda5
                  ├─sda6
                  └─sda7
OCZ-AGILITY3      sdb
                  ├─sdb1
                  ├─sdb2
                  ├─sdb3
                  ├─sdb4
                  ├─sdb5
                  ├─sdb6
                  └─sdb7
```
```
[root@sysrescue ~]# lsblk -o NAME,SIZE,MOUNTPOINT
NAME     SIZE MOUNTPOINT
loop0  626.8M /run/archiso/sfs/airootfs
sda    465.8G 
├─sda1   486M 
├─sda2  92.7G 
├─sda3  18.6G 
├─sda4     1K 
├─sda5  37.3G 
├─sda6   3.7G 
└─sda7   313G 
sdb     55.9G 
├─sdb1   486M 
├─sdb2  41.4G 
├─sdb3     1M 
├─sdb4     1K 
├─sdb5   1.2M 
├─sdb6   976K 
└─sdb7    14G 
```
**Model placa mare, quantitat de memória, model i any cpu**
```
[root@sysrescue ~]# dmidecode -s baseboard-product-name
H61M-S2PV REV 2.2
```

```
[root@sysrescue ~]# dmidecode -s baseboard-manufacturer
Gigabyte Technology Co., Ltd.
```

Slots ocupados  modelo RAM


**Slots ocupados:**
```
[root@sysrescue ~]# dmidecode -t memory
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

Handle 0x0040, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown

Handle 0x0041, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM0
	Bank Locator: BANK 0
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1333 MT/s
	Manufacturer: Kingston
	Serial Number: 65165975
	Asset Tag: 9876543210
	Part Number: 99U5474-016.A00LF 
	Rank: 1
	Configured Memory Speed: 1333 MT/s

```


```
[root@sysrescue ~]# dmidecode -t 16
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2
    ```








   Para ampliar RAM: Mirar que ddr para que sea compatible con las demas ram 


   smartctl --all /dev/sda   o sdb     info extra del diso

   