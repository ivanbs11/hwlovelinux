## 11 nov 

## Como extraer info de HARDWARE: UN lshw
```
 [root@a25 ~]# lshw      
       description: Motherboard
       product: H81M-S2PV
       vendor: Gigabyte Technology Co., Ltd.
       physical id: 0
       version: x.x
       serial: To be filled by O.E.M.
       slot: To be filled by O.E.M.
     *-firmware
          description: BIOS
          vendor: American Megatrends Inc.
          physical id: 0
          version: F5
          date: 01/17/2014
          size: 64KiB
          capacity: 8MiB
          capabilities: pci upgrade shadowing cdboot bootselect socketedrom edd int13floppy1200 int13floppy720 int13floppy2880 int5printscreen int9keyboard int14serial int17printer acpi usb biosbootspecification uefi
     *-cache:0
          description: L1 cache
          physical id: 4
          slot: CPU Internal L1
          size: 128KiB
          capacity: 128KiB
          capabilities: internal write-back
          configuration: level=1
     *-cache:1
          description: L2 cache
          physical id: 5
          slot: CPU Internal L2
          size: 512KiB
          capacity: 512KiB
          capabilities: internal write-back unified
          configuration: level=2
     *-cache:2
          description: L3 cache
          physical id: 6
          slot: CPU Internal L3
          size: 3MiB
          capacity: 3MiB
          capabilities: internal write-back unified
          configuration: level=3
     *-memory
          description: System Memory
          physical id: 7
          slot: System board or motherboard
          size: 8GiB
        *-bank:0
             description: DIMM DDR3 Synchronous 1400 MHz (0.7 ns)
             product: 9905584-014.A00LF
             vendor: Kingston
             physical id: 0
             serial: 44561214
             slot: ChannelA-DIMM0
             size: 4GiB
             width: 64 bits
             clock: 1400MHz (0.7ns)
        *-bank:1
             description: DIMM [empty]
             product: [Empty]
             vendor: [Empty]
             physical id: 1
             serial: [Empty]
             slot: ChannelA-DIMM1
        *-bank:2
             description: DIMM DDR3 Synchronous 1400 MHz (0.7 ns)
             product: 9905584-014.A00LF
             vendor: Kingston
             physical id: 2
             serial: 43561519
             slot: ChannelB-DIMM0
             size: 4GiB
             width: 64 bits
             clock: 1400MHz (0.7ns)
        *-bank:3
             description: DIMM [empty]
             product: [Empty]
             vendor: [Empty]
             physical id: 3
             serial: [Empty]
             slot: ChannelB-DIMM1
     *-cpu
          description: CPU
          product: Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
          vendor: Intel Corp.
          physical id: 40
          bus info: cpu@0
          version: Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
          slot: SOCKET 0
          size: 3190MHz
          capacity: 3200MHz
          width: 64 bits
          clock: 100MHz
          capabilities: lm fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp x86-64 constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg cx16 xtpr pdcm pcid sse4_1 sse4_2 movbe popcnt tsc_deadline_timer xsave rdrand lahf_lm abm cpuid_fault invpcid_single pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid ept_ad fsgsbase tsc_adjust erms invpcid xsaveopt dtherm arat pln pts md_clear flush_l1d cpufreq
          configuration: cores=2 enabledcores=1
     *-pci
          description: Host bridge
          product: 4th Gen Core Processor DRAM Controller
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 06
          width: 32 bits
          clock: 33MHz
          configuration: driver=hsw_uncore
          resources: irq:0
        *-display
             description: VGA compatible controller
             product: Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller
             vendor: Intel Corporation
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 06
             width: 64 bits
             clock: 33MHz
             capabilities: msi pm vga_controller bus_master cap_list rom
             configuration: driver=i915 latency=0
             resources: irq:30 memory:f0000000-f03fffff memory:e0000000-efffffff ioport:f000(size=64) memory:c0000-dffff
        *-multimedia:0
             description: Audio device
             product: Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller
             vendor: Intel Corporation
             physical id: 3
             bus info: pci@0000:00:03.0
             version: 06
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi pciexpress bus_master cap_list
             configuration: driver=snd_hda_intel latency=0
             resources: irq:32 memory:f0514000-f0517fff
        *-usb:0
             description: USB controller
             product: 8 Series/C220 Series Chipset Family USB xHCI
             vendor: Intel Corporation
             physical id: 14
             bus info: pci@0000:00:14.0
             version: 05
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi xhci bus_master cap_list
             configuration: driver=xhci_hcd latency=0
             resources: irq:28 memory:f0500000-f050ffff
           *-usbhost:0
                product: xHCI Host Controller
                vendor: Linux 5.8.15-201.fc32.x86_64 xhci-hcd
                physical id: 0
                bus info: usb@3
                logical name: usb3
                version: 5.08
                capabilities: usb-2.00
                configuration: driver=hub slots=10 speed=480Mbit/s
              *-usb:0
                   description: Keyboard
                   product: USB Keyboard
                   vendor: Logitech
                   physical id: 3
                   bus info: usb@3:3
                   version: 64.00
                   capabilities: usb-1.10
                   configuration: driver=usbhid maxpower=90mA speed=2Mbit/s
              *-usb:1
                   description: Mouse
                   product: USB Optical Mouse
                   vendor: Logitech
                   physical id: 4
                   bus info: usb@3:4
                   version: 63.00
                   capabilities: usb-2.00
                   configuration: driver=usbhid maxpower=100mA speed=2Mbit/s
           *-usbhost:1
                product: xHCI Host Controller
                vendor: Linux 5.8.15-201.fc32.x86_64 xhci-hcd
                physical id: 1
                bus info: usb@4
                logical name: usb4
                version: 5.08
                capabilities: usb-3.00
                configuration: driver=hub slots=2 speed=5000Mbit/s
        *-communication
             description: Communication controller
             product: 8 Series/C220 Series Chipset Family MEI Controller #1
             vendor: Intel Corporation
             physical id: 16
             bus info: pci@0000:00:16.0
             version: 04
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi bus_master cap_list
             configuration: driver=mei_me latency=0
             resources: irq:31 memory:f051e000-f051e00f
        *-usb:1
             description: USB controller
             product: 8 Series/C220 Series Chipset Family USB EHCI #2
             vendor: Intel Corporation
             physical id: 1a
             bus info: pci@0000:00:1a.0
             version: 05
             width: 32 bits
             clock: 33MHz
             capabilities: pm debug ehci bus_master cap_list
             configuration: driver=ehci-pci latency=0
             resources: irq:16 memory:f051c000-f051c3ff
           *-usbhost
                product: EHCI Host Controller
                vendor: Linux 5.8.15-201.fc32.x86_64 ehci_hcd
                physical id: 1
                bus info: usb@1
                logical name: usb1
                version: 5.08
                capabilities: usb-2.00
                configuration: driver=hub slots=2 speed=480Mbit/s
              *-usb
                   description: USB hub
                   product: Integrated Rate Matching Hub
                   vendor: Intel Corp.
                   physical id: 1
                   bus info: usb@1:1
                   version: 0.05
                   capabilities: usb-2.00
                   configuration: driver=hub slots=4 speed=480Mbit/s
        *-multimedia:1
             description: Audio device
             product: 8 Series/C220 Series Chipset High Definition Audio Controller
             vendor: Intel Corporation
             physical id: 1b
             bus info: pci@0000:00:1b.0
             version: 05
             width: 64 bits
             clock: 33MHz
             capabilities: pm msi pciexpress bus_master cap_list
             configuration: driver=snd_hda_intel latency=0
             resources: irq:33 memory:f0510000-f0513fff
        *-pci:0
             description: PCI bridge
             product: 8 Series/C220 Series Chipset Family PCI Express Root Port #1
             vendor: Intel Corporation
             physical id: 1c
             bus info: pci@0000:00:1c.0
             version: d5
             width: 32 bits
             clock: 33MHz
             capabilities: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:24
        *-pci:1
             description: PCI bridge
             product: 8 Series/C220 Series Chipset Family PCI Express Root Port #3
             vendor: Intel Corporation
             physical id: 1c.2
             bus info: pci@0000:00:1c.2
             version: d5
             width: 32 bits
             clock: 33MHz
             capabilities: pci pciexpress msi pm normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:25 ioport:e000(size=4096) memory:f0400000-f04fffff
           *-network
                description: Ethernet interface
                product: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
                vendor: Realtek Semiconductor Co., Ltd.
                physical id: 0
                bus info: pci@0000:02:00.0
                logical name: enp2s0
                version: 06
                serial: 74:d4:35:a1:0e:16
                size: 1Gbit/s
                capacity: 1Gbit/s
                width: 64 bits
                clock: 33MHz
                capabilities: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
                configuration: autonegotiation=on broadcast=yes driver=r8169 driverversion=5.8.15-201.fc32.x86_64 duplex=full firmware=rtl8168e-3_0.0.4 03/27/12 ip=10.200.246.225 latency=0 link=yes multicast=yes port=MII speed=1Gbit/s
                resources: irq:18 ioport:e000(size=256) memory:f0404000-f0404fff memory:f0400000-f0403fff
        *-pci:2
             description: PCI bridge
             product: 82801 PCI Bridge
             vendor: Intel Corporation
             physical id: 1c.3
             bus info: pci@0000:00:1c.3
             version: d5
             width: 32 bits
             clock: 33MHz
             capabilities: pci pciexpress msi pm subtractive_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:26
           *-pci
                description: PCI bridge
                product: 82801 PCI Bridge
                vendor: Intel Corporation
                physical id: 0
                bus info: pci@0000:03:00.0
                version: 41
                width: 32 bits
                clock: 33MHz
                capabilities: pci pm subtractive_decode bus_master cap_list
        *-usb:2
             description: USB controller
             product: 8 Series/C220 Series Chipset Family USB EHCI #1
             vendor: Intel Corporation
             physical id: 1d
             bus info: pci@0000:00:1d.0
             version: 05
             width: 32 bits
             clock: 33MHz
             capabilities: pm debug ehci bus_master cap_list
             configuration: driver=ehci-pci latency=0
             resources: irq:23 memory:f051b000-f051b3ff
           *-usbhost
                product: EHCI Host Controller
                vendor: Linux 5.8.15-201.fc32.x86_64 ehci_hcd
                physical id: 1
                bus info: usb@2
                logical name: usb2
                version: 5.08
                capabilities: usb-2.00
                configuration: driver=hub slots=2 speed=480Mbit/s
              *-usb
                   description: USB hub
                   product: Integrated Rate Matching Hub
                   vendor: Intel Corp.
                   physical id: 1
                   bus info: usb@2:1
                   version: 0.05
                   capabilities: usb-2.00
                   configuration: driver=hub slots=6 speed=480Mbit/s
        *-isa
             description: ISA bridge
             product: H81 Express LPC Controller
             vendor: Intel Corporation
             physical id: 1f
             bus info: pci@0000:00:1f.0
             version: 05
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master cap_list
             configuration: driver=lpc_ich latency=0
             resources: irq:0
        *-sata
             description: SATA controller
             product: 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]
             vendor: Intel Corporation
             physical id: 1f.2
             bus info: pci@0000:00:1f.2
             logical name: scsi4
             version: 05
             width: 32 bits
             clock: 66MHz
             capabilities: sata msi pm ahci_1.0 bus_master cap_list emulated
             configuration: driver=ahci latency=0
             resources: irq:27 ioport:f0b0(size=8) ioport:f0a0(size=4) ioport:f090(size=8) ioport:f080(size=4) ioport:f060(size=32) memory:f051a000-f051a7ff
           *-disk
                description: ATA Disk
                product: TOSHIBA DT01ACA0
                vendor: Toshiba
                physical id: 0.0.0
                bus info: scsi@4:0.0.0
                logical name: /dev/sda
                version: A750
                serial: 56VHJ58AS
                size: 465GiB (500GB)
                capabilities: partitioned partitioned:dos
                configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=67d28bf2
              *-volume
                   description: Extended partition
                   physical id: 1
                   bus info: scsi@4:0.0.0,1
                   logical name: /dev/sda1
                   size: 205GiB
                   capacity: 205GiB
                   capabilities: primary extended partitioned partitioned:extended
                 *-logicalvolume:0
                      description: EXT4 volume
                      vendor: Linux
                      physical id: 5
                      logical name: /dev/sda5
                      logical name: /
                      version: 1.0
                      serial: a3b015f4-70db-4809-81be-4fe9de393b3e
                      size: 100GiB
                      capacity: 100GiB
                      capabilities: bootable journaled extended_attributes large_files huge_files dir_nlink 64bit extents ext4 ext2 initialized
                      configuration: created=2020-10-20 13:16:48 filesystem=ext4 label=MATI lastmountpoint=/ modified=2020-11-11 12:21:30 mount.fstype=ext4 mount.options=rw,seclabel,relatime mounted=2020-11-11 11:44:44 state=mounted
                 *-logicalvolume:1
                      description: EXT4 volume
                      vendor: Linux
                      physical id: 6
                      logical name: /dev/sda6
                      version: 1.0
                      serial: a1281d96-245f-4505-bdb6-4991c188c17e
                      size: 100GiB
                      capacity: 100GiB
                      capabilities: journaled extended_attributes large_files huge_files dir_nlink 64bit extents ext4 ext2 initialized
                      configuration: created=2019-09-13 16:34:41 filesystem=ext4 label=TARDA lastmountpoint=/ modified=2020-10-20 12:46:50 mounted=2020-10-20 12:46:54 state=clean
                 *-logicalvolume:2
                      description: Linux swap volume
                      physical id: 7
                      logical name: /dev/sda7
                      version: 1
                      serial: 017ddc0d-0216-42a9-9176-78698d9cb0d1
                      size: 5117MiB
                      capacity: 5117MiB
                      capabilities: nofs swap initialized
                      configuration: filesystem=swap pagesize=4096
        *-serial
             description: SMBus
             product: 8 Series/C220 Series Chipset Family SMBus Controller
             vendor: Intel Corporation
             physical id: 1f.3
             bus info: pci@0000:00:1f.3
             version: 05
             width: 64 bits
             clock: 33MHz
             configuration: driver=i801_smbus latency=0
             resources: irq:18 memory:f0519000-f05190ff ioport:f040(size=32)
     *-pnp00:00
          product: PnP device PNP0c01
          physical id: 1
          capabilities: pnp
          configuration: driver=system
     *-pnp00:01
          product: PnP device PNP0c02
          physical id: 2
          capabilities: pnp
          configuration: driver=system
     *-pnp00:02
          product: PnP device PNP0b00
          physical id: 3
          capabilities: pnp
          configuration: driver=rtc_cmos
     *-pnp00:03
          product: PnP device INT3f0d
          vendor: Interphase Corporation
          physical id: 8
          capabilities: pnp
          configuration: driver=system
     *-pnp00:04
          product: PnP device PNP0c02
          physical id: 9
          capabilities: pnp
          configuration: driver=system
     *-pnp00:05
          product: PnP device PNP0501
          physical id: a
          capabilities: pnp
          configuration: driver=serial
     *-pnp00:06
          product: PnP device PNP0400
          physical id: b
          capabilities: pnp
          configuration: driver=parport_pc
     *-pnp00:07
          product: PnP device PNP0c02
          physical id: c
          capabilities: pnp
          configuration: driver=system
     *-pnp00:08
          product: PnP device PNP0c02
          physical id: d
          capabilities: pnp
          configuration: driver=system
  *-power UNCLAIMED
       description: To Be Filled By O.E.M.
       product: To Be Filled By O.E.M.
       vendor: To Be Filled By O.E.M.
       physical id: 1
       version: To Be Filled By O.E.M.
       serial: To Be Filled By O.E.M.
       capacity: 32768mWh
```
##11 nov
##Información del disco:
```
[root@a25 ~]# lsblk /dev/sda
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part /
├─sda6   8:6    0   100G  0 part 
└─sda7   8:7    0     5G  0 part [SWAP]
```
## Extraer info del disco: Memory


##11 nov
## Extraer info del disco : Problemas
```
[root@a25 ~]# smartctl -H /dev/sda
smartctl 7.1 2019-12-30 r5022 [x86_64-linux-5.8.15-201.fc32.x86_64] (local build)
Copyright (C) 2002-19, Bruce Allen, Christian Franke, www.smartmontools.org

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED

```
##11 nov
## Extraer info del del disco: lshw
```
[root@a25 ~]# lshw -class disk
  *-disk                    
       description: ATA Disk
       product: TOSHIBA DT01ACA0
       vendor: Toshiba
       physical id: 0.0.0
       bus info: scsi@4:0.0.0
       logical name: /dev/sda
       version: A750
       serial: 56VHJ58AS
       size: 465GiB (500GB)
       capabilities: partitioned partitioned:dos
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=67d28bf2
```
## esto es la info del disco con lshw, lo filtras con la palbara lshw más, se mira haciendo un lshw --help, graciaas al manual lo he encontrado: 
```
[root@a25 ~]# lshw --help
Hardware Lister (lshw) - B.02.19.2
usage: lshw [-format] [-options ...]
       lshw -version

	-version        print program version (B.02.19.2)

format can be
	-html           output hardware tree as HTML
	-xml            output hardware tree as XML
	-json           output hardware tree as a JSON object
	-short          output hardware paths
	-businfo        output bus information

options can be
	-class CLASS    only show a certain class of hardware
	-C CLASS        same as '-class CLASS'
	-c CLASS        same as '-class CLASS'
	-disable TEST   disable a test (like pci, isapnp, cpuid, etc. )
	-enable TEST    enable a test (like pci, isapnp, cpuid, etc. )
	-quiet          don't display status
	-sanitize       sanitize output (remove sensitive information like serial numbers, etc.)
	-numeric        output numeric IDs (for PCI, USB, etc.)
	-notime         exclude volatile attributes (timestamps) from output

```
## después de mirar el manual, hay una opción que es muy especifica para mirar el disco:
#lshw -class disk
```
[root@a25 ~]# lshw -class disk
  *-disk                    
       description: ATA Disk
       product: TOSHIBA DT01ACA0
       vendor: Toshiba
       physical id: 0.0.0
       bus info: scsi@4:0.0.0
       logical name: /dev/sda
       version: A750
       serial: 56VHJ58AS
       size: 465GiB (500GB)
       capabilities: partitioned partitioned:dos
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=67d28bf2
```
## info sobre memory con dmidecode
```
[root@a25 ~]# dmidecode -t memory
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

Handle 0x0041, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM0
	Bank Locator: BANK 0
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1400 MT/s
	Manufacturer: Kingston
	Serial Number: 44561214
	Asset Tag: 9876543210
	Part Number: 9905584-014.A00LF 
	Rank: 1
	Configured Memory Speed: 1400 MT/s

Handle 0x0043, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelA-DIMM1
	Bank Locator: BANK 1
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown

Handle 0x0044, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 4 GB
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: DDR3
	Type Detail: Synchronous
	Speed: 1400 MT/s
	Manufacturer: Kingston
	Serial Number: 43561519
	Asset Tag: 9876543210
	Part Number: 9905584-014.A00LF 
	Rank: 1
	Configured Memory Speed: 1400 MT/s

Handle 0x0046, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM1
	Bank Locator: BANK 3
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown
```
## este comando es para ver informacióón sobre la memory con el dmidecode
