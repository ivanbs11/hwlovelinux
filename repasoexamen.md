**Tarjetas pci conectadas**
```
[root@sysrescue ~]# lspci -nn

00:00.0 Host bridge [0600]: Intel Corporation Xeon E3-1200 v2/3rd Gen Core processor DRAM Controller [8086:0150] (rev 09)
00:02.0 VGA compatible controller [0300]: Intel Corporation Xeon E3-1200 v2/3rd Gen Core processor Graphics Controller [8086:0152] (rev 09)
00:16.0 Communication controller [0780]: Intel Corporation 6 Series/C200 Series Chipset Family MEI Controller #1 [8086:1c3a] (rev 04)
00:1a.0 USB controller [0c03]: Intel Corporation 6 Series/C200 Series Chipset Family USB Enhanced Host Controller #2 [8086:1c2d] (rev 05)
00:1b.0 Audio device [0403]: Intel Corporation 6 Series/C200 Series Chipset Family High Definition Audio Controller [8086:1c20] (rev 05)
00:1c.0 PCI bridge [0604]: Intel Corporation 6 Series/C200 Series Chipset Family PCI Express Root Port 1 [8086:1c10] (rev b5)
00:1c.4 PCI bridge [0604]: Intel Corporation 6 Series/C200 Series Chipset Family PCI Express Root Port 5 [8086:1c18] (rev b5)
00:1c.5 PCI bridge [0604]: Intel Corporation 82801 PCI Bridge [8086:244e] (rev b5)

```
**Información más especifica:**
```
lspci -nn -k -mm -v
[root@f32-isard ~]# lspci -nn -k -mm -v
Slot:	00:00.0
Class:	Host bridge [0600]
Vendor:	Intel Corporation [8086]
Device:	82G33/G31/P35/P31 Express DRAM Controller [29c0]
SVendor:	Red Hat, Inc. [1af4]
SDevice:	QEMU Virtual Machine [1100]


```
**Model dels disc i capacitats**
```
[root@sysrescue ~]# df
Filesystem     1K-blocks   Used Available Use% Mounted on
dev              1934448      0   1934448   0% /dev
run              1960980  95784   1865196   5% /run
httpspace        2941476 641796   2299680  22% /run/archiso/httpspace
copytoram        2941476 641792   2299684  22% /run/archiso/copytoram
cowspace          980492    968    979524   1% /run/archiso/cowspace

```
```
[root@sysrescue ~]# lsblk -o MODEL,NAME
MODEL             NAME
                  loop0
ST500DM002-1BD142 sda
                  ├─sda1
                  ├─sda2
                  ├─sda3
                  ├─sda4
                  ├─sda5
                  ├─sda6
                  └─sda7
OCZ-AGILITY3      sdb
                  ├─sdb1
                  ├─sdb2
                  ├─sdb3
                  ├─sdb4
                  ├─sdb5
                  ├─sdb6
                  └─sdb7

```

```
[root@sysrescue ~]# lsblk -o NAME,SIZE,MOUNTPOINT
NAME     SIZE MOUNTPOINT
loop0  626.8M /run/archiso/sfs/airootfs
sda    465.8G 
├─sda1   486M 
├─sda2  92.7G 
├─sda3  18.6G 
├─sda4     1K 
├─sda5  37.3G 
├─sda6   3.7G 
└─sda7   313G 
sdb     55.9G 
├─sdb1   486M 
├─sdb2  41.4G 
├─sdb3     1M 
├─sdb4     1K 
├─sdb5   1.2M 
├─sdb6   976K 
└─sdb7    14G
```

**Modelo placa base**
```
[root@sysrescue ~]# dmidecode -s baseboard-product-name
H61M-S2PV REV 2.2

[root@sysrescue ~]# dmidecode -s baseboard-manufacturer
Gigabyte Technology Co., Ltd.
```
**Slots ocupados, slots, cantidad de memoria**
```
[root@sysrescue ~]# dmidecode -t memory
# dmidecode 3.3
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

Handle 0x0040, DMI type 17, 34 bytes
Memory Device
	Array Handle: 0x0007
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: ChannelB-DIMM0
	Bank Locator: BANK 2
	Type: Unknown
	Type Detail: None
	Speed: Unknown
	Manufacturer: [Empty]
	Serial Number: [Empty]
	Asset Tag: 9876543210
	Part Number: [Empty]
	Rank: Unknown
	Configured Memory Speed: Unknown
```
**info extra del diso**
```
smartctl --all /dev/sda   (o sdb)
```
**cores, threads,socket,cache**
```
[root@a25 ~]# lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          2
On-line CPU(s) list:             0,1
Thread(s) per core:              1
Core(s) per socket:              2
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           60
Model name:                      Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Stepping:                        3
CPU MHz:                         1338.547
CPU max MHz:                     3200.0000
CPU min MHz:                     800.0000
BogoMIPS:                        6385.11
Virtualization:                  VT-x
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB
```
**number of pci slots, lanes available**
```
[root@a25 ~]# dmidecode -t 9
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0020, DMI type 9, 17 bytes
System Slot Information
	Designation: J6B2
	Type: x16 PCI Express
	Current Usage: In Use
	Length: Long
	ID: 0
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:02.0

Handle 0x0021, DMI type 9, 17 bytes
System Slot Information
	Designation: J6B1
	Type: x1 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 1
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:1c.0

Handle 0x0022, DMI type 9, 17 bytes
System Slot Information
	Designation: J6D1
	Type: x8 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 2
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:01.0

Handle 0x0023, DMI type 9, 17 bytes
System Slot Information
	Designation: J7B1
	Type: x16 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 3
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:03.0

Handle 0x0024, DMI type 9, 17 bytes
System Slot Information
	Designation: J8B4
	Type: x1 PCI Express
	Current Usage: In Use
	Length: Short
	ID: 4
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:1c.7

Handle 0x0025, DMI type 9, 17 bytes
System Slot Information
	Designation: J8B3
	Type: 32-bit PCI
	Current Usage: In Use
	Length: Short
	ID: 6
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:14:1e.0
```
**SYSTem model**
```
[root@f32-isard ~]# dmidecode -t system
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0100, DMI type 1, 27 bytes
System Information
	Manufacturer: QEMU
	Product Name: Standard PC (Q35 + ICH9, 2009)
	Version: pc-q35-5.1
	Serial Number: Not Specified
	UUID: acf6166f-f23b-4030-9ae8-2374e807cea9
	Wake-up Type: Power Switch
	SKU Number: Not Specified
	Family: Not Specified

Handle 0x2000, DMI type 32, 11 bytes
System Boot Information
	Status: No errors detected
```

**bios version and bios date**
```
[root@f32-isard ~]# dmidecode -t bios
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0000, DMI type 0, 24 bytes
BIOS Information
	Vendor: SeaBIOS
	Version: rel-1.13.0-48-gd9c812dda519-prebuilt.qemu.org
	Release Date: 04/01/2014
	Address: 0xE8000
	Runtime Size: 96 kB
	ROM Size: 64 kB
	Characteristics:
		BIOS characteristics not supported
		Targeted content distribution is supported
	BIOS Revision: 0.0
```
**cpu model**se usa dmidecode para ver la versión y modelo de la cpu:
```
[root@a25 ~]# dmidecode -s processor-version
Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
```
```
[root@a25 ~]# dmidecode -s processor-manufacturer
Intel
```
**Discos conectados**
```
[hw@nvidia-t4 ~]$ lsscsi
[0:0:0:0]    disk    ATA      KINGSTON SHFS37A BBF0  /dev/sda 
[3:0:0:0]    disk    ATA      KINGSTON SHFS37A BBF0  /dev/sdb
```
**Tarjetas de video**
```
[root@f32-isard ~]# lspci | grep -i vga
00:01.0 VGA compatible controller: Red Hat, Inc. QXL paravirtual graphic card (rev 05)
```
**tarjeta controladora de discos**
```
[root@f32-isard ~]# lspci -vm | grep -i sata
Class:	SATA controller
Device:	82801IR/IO/IH (ICH9R/DO/DH) 6 port SATA Controller [AHCI mode]
```
**tipo y modelo discos duros:**
```
[root@f32-isard ~]# lshw -class disk
  *-virtio2                 
       description: Virtual I/O device
       physical id: 0
       bus info: virtio@2
       logical name: /dev/vda
       size: 60GiB (64GB)
       capabilities: partitioned partitioned:dos
       configuration: driver=virtio_blk logicalsectorsize=512 sectorsize=512 signature=13bd811d
```
[https://www.profesionalreview.com/2018/04/01/como-verificar-la-informacion-sobre-el-hardware-en-linux/]
```
[root@f32-isard ~]# lspci | grep -i vga
00:01.0 VGA compatible controller: Red Hat, Inc. QXL paravirtual graphic card (rev 05)

[root@f32-isard ~]# lspci | grep -i audio
00:1b.0 Audio device: Intel Corporation 82801I (ICH9 Family) HD Audio Controller (rev 03)
```